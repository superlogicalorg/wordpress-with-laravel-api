<?php
class LinuxServices{

	public function executeCommand($cmd="ping -c 3 google.com", $desc="" ){
	
		while (@ ob_end_flush()); // end all output buffers if any
	
		$proc = popen($cmd, 'r');
		
		echo '<div style="width: 100%; margin-top: 100px;">';
		
			echo '<div style="width: 60%; margin: auto">';
			
				echo "<h3> $desc </h3>";
				echo '<pre style="color: green">';
					while (!feof($proc))
					{
					    echo fread($proc, 4096);
					    @ flush();
					}
				echo '</pre>';
				
		echo '</div></div>';
		
	}
	
	
	
	public function executeCommands($cmds, $desc=""){
		
		$cmdString = "";
		
		for($i=0; $i<count($cmds); $i++){
		
			$cmdString .= $cmds[$i];
			if($i < count($cmds)-1 ){
				$cmdString .= " && ";
			}
		
		}
		
		$this->executeCommand($cmdString, $desc);
		
	}

	public function ok(){
		return "ok";
	}

}
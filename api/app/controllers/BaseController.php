<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}



	protected function login(){

		//$creds = array('user_email' => Input::get('email'), 'laravel_pass' => Input::get('password'));
		$userdata = array(
		    'user_login' 	=> Input::get('log'),
		    'password' 		=> Input::get('pwd')
		);

		if (Auth::attempt($userdata))
		{
		    // successfully logged in
		    //print_r();
		    
			Auth::loginUsingId(Auth::user()->ID);
			$_SESSION['user']['id'] = Auth::user()->ID;
		    return 'Logged in';
		}
		else
		{
		    // authentication failed
		    return "authentication failed";
		}

		

	}

}

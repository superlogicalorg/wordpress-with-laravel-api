<?php

class WpPostsController extends BaseController {

	/**
	 * WpPost Repository
	 *
	 * @var WpPost
	 */
	protected $WpPost;

	public function __construct(WpPost $WpPost)
	{
		$this->WpPost = $WpPost;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$WpPosts = $this->WpPost->all();

		return View::make('WpPosts.index', compact('WpPosts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('WpPosts.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, WpPost::$rules);

		if ($validation->passes())
		{
			$this->WpPost->create($input);

			return Redirect::route('api.wp.posts.index');
		}

		return Redirect::route('WpPosts.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$WpPost = $this->WpPost->findOrFail($id);

		return View::make('WpPosts.show', compact('WpPost'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$WpPost = $this->WpPost->find($id);

		if (is_null($WpPost))
		{
			return Redirect::route('api.wp.posts.index');
		}

		return View::make('WpPosts.edit', compact('WpPost'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, WpPost::$rules);

		if ($validation->passes())
		{
			$WpPost = $this->WpPost->find($id);
			$WpPost->update($input);

			return Redirect::route('api.wp.posts.show', $id);
		}

		return Redirect::route('api.wp.posts.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->WpPost->find($id)->delete();

		return Redirect::route('api.wp.posts.index');
	}

}

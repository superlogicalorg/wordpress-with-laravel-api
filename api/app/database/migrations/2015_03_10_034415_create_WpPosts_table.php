<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWpPostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('WpPosts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->bigint('post_author', 20);
			$table->datetime('post_date');
			$table->text('post_title');
			$table->longtext('post_content');
			$table->varchar('post_status', 20);
			$table->varchar('comment_status', 20);
			$table->int('menu_order', 11);
			$table->varchar('post_type', 20);
			$table->bigint('comment_count', 20);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('WpPosts');
	}

}

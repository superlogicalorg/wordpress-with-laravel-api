<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWpPostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('WpPosts', function(Blueprint $table) {
			$table->increments('id');
			$table->bigint(20)('post_author');
			$table->datetime('post_date');
			$table->text('post_title');
			$table->longtext('post_content');
			$table->varchar(20)('post_status');
			$table->varchar(20)('comment_status');
			$table->int(11)('menu_order');
			$table->varchar(20)('post_type');
			$table->bigint(20)('comment_count');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('WpPosts');
	}

}

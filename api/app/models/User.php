<?php

// use Illuminate\Auth\UserTrait;
// use Illuminate\Auth\UserInterface;
// use Illuminate\Auth\Reminders\RemindableTrait;
// use Illuminate\Auth\Reminders\RemindableInterface;

// class User extends Eloquent implements UserInterface, RemindableInterface {

// 	use UserTrait, RemindableTrait;

// 	/**
// 	 * The database table used by the model.
// 	 *
// 	 * @var string
// 	 */
// 	protected $table = 'users';

// 	/**
// 	 * The attributes excluded from the model's JSON form.
// 	 *
// 	 * @var array
// 	 */
// 	protected $hidden = array('password', 'remember_token');

// }



class User extends Eloquent {

    // WordPress uses differently named fields for create and update fields than Laravel does
    const CREATED_AT = 'post_date';
    const UPDATED_AT = 'post_modified';
    
    // Set the table including database prefix used in WordPress
    protected $table = 'wp_users';

    // WordPress uses uppercase "ID" for the primary key
    protected $primaryKey = 'ID';

    // Hide the user_pass field
    protected $hidden = array('user_pass');

    // Whenever the user_pass field is modified, WordPress' internal hashing function will run
    public function setUserPassAttribute($pass)
    {
        $this->attributes['user_pass'] = wp_hash_password($pass);
    }

}

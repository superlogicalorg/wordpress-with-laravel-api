<?php

class WpPost extends Eloquent {
	protected $guarded = array();
	protected $primaryKey = 'ID';
	public static $rules = array(
		'post_author' => 'required',
		'post_date' => 'required',
		'post_title' => 'required',
		'post_content' => 'required',
		'post_status' => 'required',
		'comment_status' => 'required',
		'menu_order' => 'required',
		'post_type' => 'required',
		'comment_count' => 'required'
	);
}

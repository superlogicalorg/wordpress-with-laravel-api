<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
//session_start();

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('/api/test', function()
{
	//$email = Auth::user();
	//print_r($_SESSION['user']['id']);
	print_r(wp_get_current_user());

});


Route::any('/api/login', 'BaseController@login');

// =========== WORDPRESS =================
Route::resource('api/wp/posts', 'WpPostsController');
//Route::resource('api/wpposts', 'WpPostsController');


Route::get('/api/test_cmd', function(){
	$linuxService = new LinuxServices();
	$linuxService->executeCommand();
});
@extends('layouts.scaffold')

@section('main')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Create WpPost</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::open(array('route' => 'api.wp.posts.store', 'class' => 'form-horizontal')) }}

        <div class="form-group">
            {{ Form::label('post_author', 'Post_author:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('post_author', Input::old('post_author'), array('class'=>'form-control', 'placeholder'=>'Post_author')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('post_date', 'Post_date:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('post_date', Input::old('post_date'), array('class'=>'form-control', 'placeholder'=>'Post_date')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('post_title', 'Post_title:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::textarea('post_title', Input::old('post_title'), array('class'=>'form-control', 'placeholder'=>'Post_title')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('post_content', 'Post_content:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('post_content', Input::old('post_content'), array('class'=>'form-control', 'placeholder'=>'Post_content')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('post_status', 'Post_status:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('post_status', Input::old('post_status'), array('class'=>'form-control', 'placeholder'=>'Post_status')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('comment_status', 'Comment_status:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('comment_status', Input::old('comment_status'), array('class'=>'form-control', 'placeholder'=>'Comment_status')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('menu_order', 'Menu_order:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('menu_order', Input::old('menu_order'), array('class'=>'form-control', 'placeholder'=>'Menu_order')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('post_type', 'Post_type:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('post_type', Input::old('post_type'), array('class'=>'form-control', 'placeholder'=>'Post_type')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('comment_count', 'Comment_count:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
              {{ Form::text('comment_count', Input::old('comment_count'), array('class'=>'form-control', 'placeholder'=>'Comment_count')) }}
            </div>
        </div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}

@stop



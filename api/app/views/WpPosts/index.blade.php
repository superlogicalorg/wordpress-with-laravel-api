@extends('layouts.scaffold')

@section('main')

<h1>All WpPosts</h1>

<p>{{ link_to_route('api.wp.posts.create', 'Add New WpPost', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($WpPosts->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Post_author</th>
				<th>Post_date</th>
				<th>Post_title</th>
				<th>Post_content</th>
				<th>Post_status</th>
				<th>Comment_status</th>
				<th>Menu_order</th>
				<th>Post_type</th>
				<th>Comment_count</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($WpPosts as $WpPost)
				<tr>
					<td>{{{ $WpPost->post_author }}}</td>
					<td>{{{ $WpPost->post_date }}}</td>
					<td>{{{ $WpPost->post_title }}}</td>
					<td>{{{ $WpPost->post_content }}}</td>
					<td>{{{ $WpPost->post_status }}}</td>
					<td>{{{ $WpPost->comment_status }}}</td>
					<td>{{{ $WpPost->menu_order }}}</td>
					<td>{{{ $WpPost->post_type }}}</td>
					<td>{{{ $WpPost->comment_count }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('api.wp.posts.destroy', $WpPost->ID))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('api.wp.posts.edit', 'Edit', array($WpPost->ID), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no WpPosts
@endif

@stop

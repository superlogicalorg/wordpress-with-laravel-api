<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mys092500');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R`|f$-x+[`)4JCE9>M[;V=-%BQq8rUfv=2+Va/$O%<(UP<0tOVB@:so[BLcR~3]8');
define('SECURE_AUTH_KEY',  'UJZ+m<g89|1_~wz$%%{D[J8<v?$N4VVL]g4-6F;)Ap,C!SH::bh&A!a6<@`U+ey*');
define('LOGGED_IN_KEY',    'DQPdkt&VC5~U==8qTb2[oLb+kQURoo/#@x$0%pHl!y_:cWUVJqW.;.kltZ9/]h9f');
define('NONCE_KEY',        't+R}JgG-fRuNDl@O|i]p/Q*R@>4:HR_2mu=^$?g:,teXLufY/`sAD|swQ}~f7|H ');
define('AUTH_SALT',        'uW9~^7B#6]vg[j!3X4^fv %7l)3W,TE(z`!#.GVg|__Iz4t5_+U7{?}CeY+USs<i');
define('SECURE_AUTH_SALT', '}o^1QGO56eM)v)Q3<q<&xX8_xZ)#n:9f{,cs9d))KLJzt:^$p@Oimf/|7eK2%f_+');
define('LOGGED_IN_SALT',   'A]>BqU!Kz(l+(Ms/v-j^|=nT6C@ScI>,{3h8u07GUhhI}HB*nIr=ZE?KH&WxvI;&');
define('NONCE_SALT',       't;Lev+.qSQl#%P7Oca-{mR+wAIRXcOn361Jbq;_m]gJYjTke4`Q7t9lS+h42-;e:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
